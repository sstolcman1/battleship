using System.Linq.Expressions;
using BattleshipGame.Library.CoordsItems;
using NUnit.Framework;

namespace BattleshipGame.Tests;


public class CoordsTests
{
    [Test]
    public void t1()
    {
        Func<int, string> toUpper = str => str.ToString().ToUpper();
        Expression<Func<string, string>> toUpperExpr = str => str.ToUpper();
        var result = toUpper(12);
        Assert.AreEqual("12", result);
Assert.Pass();


    }

    [Test]
    public void t2()
    {
        var prm = Expression.Parameter(typeof(string), "str");
        var toUpper = typeof(string).GetMethod("ToUpper", Type.EmptyTypes);
        var body = Expression.Call(prm, toUpper);
        var lambda = Expression.Lambda<Func<string, string>>(body, prm);
        var result= lambda.Compile().Invoke("spencer");
        Assert.AreEqual("SPENCER", result);
    }

    [Test]
    public void TwoCoordsWithTheSameColumnAndRowShouldBeEqual()
    {
        Coords c1 = new Coords('A', 7);
        Coords c2 = new Coords('A', 7);
        Assert.AreEqual(c1, c2);
        Assert.True(c1 == c2);
    }

    [Test]
    public void Copy_ShouldReturnNewInstanceWithTheSameColumnAndRow()
    {
        Coords c1 = new Coords('A', 7);
        var c2 = Coords.Copy(c1);
        Assert.AreEqual(c1, c2);

        c1.Column = 'B';
        Assert.AreNotEqual(c1, c2);
    }
}