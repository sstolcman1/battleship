using BattleshipGame.Library.GameItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Tests;

public class GameTests
{
    private Game _game;
    private readonly uint _boardSize = 10;
    [SetUp]
    public void Setup()
    {
        _game = new Game();
    }
    [Test]
    public void CreateGame_ShouldPass()
    {
        _game.CreateGame();
        Assert.Pass();
    }
    [Test]
    public void CreateBoards_ShouldCreateBoardAtGivenSize()
    {
        _game.CreateGame();
        var boards = _game.CreateBoards(_boardSize);
        Assert.AreEqual(boards.computerBoard.Size, _boardSize);
        Assert.AreEqual(boards.playerBoard.Size, _boardSize);
        Assert.Pass();
    }
    [Test]
    public void CreateShips_ShouldPass()
    {
        _game.CreateGame();
        var boards = _game.CreateBoards(_boardSize);
        _game.CreateShips();
        Assert.Pass();
    }
    [Test]
    public void PlaceComputerShips_ShouldPlaceShipsOnComputerBoard()
    {
        _game.CreateGame();
        var boards = _game.CreateBoards(_boardSize);
        _game.CreateShips();
        _game.PlaceComputerShips();
        Assert.AreEqual(boards.computerBoard.Ships.Count, 3);
        Assert.AreEqual(boards.computerBoard.Ships.Where(s=>s is Battleship).Count(), 1);
        Assert.AreEqual(boards.computerBoard.Ships.Where(s=>s is Destroyer).Count(), 2);
        
        Assert.False(boards.playerBoard.Ships.Any());
        
        Assert.Pass();
    }
}