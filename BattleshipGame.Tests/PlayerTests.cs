using BattleshipGame.Library.Abstraction;
using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.PlayerItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Tests;

public class PlayerTests
{
    [Test]
    public void CreateFleet_ShouldCreateListOfGivenShips()
    {
        var player = new Player();
        var battleship = new Battleship();
        var destroyer1 = new Destroyer();
        var destroyer2 = new Destroyer();

        var ships = new IShip[] {battleship, destroyer1, destroyer2};
        player.CreateFleet(battleship, destroyer1, destroyer2);

        var ship = player.GetFirstOrDefaultNotLocatedShip();
        Assert.NotNull(ship);
        Assert.True(ships.Any(s=>s == ship));
        DestroyShip(ship);
        
        var ship2 = player.GetFirstOrDefaultNotLocatedShip();
        Assert.AreNotEqual(ship, ship2);
        Assert.NotNull(ship2);
        Assert.True(ships.Any(s=>s == ship2));
        DestroyShip(ship2);

        var ship3 = player.GetFirstOrDefaultNotLocatedShip();
        Assert.AreNotEqual(ship, ship3);
        Assert.AreNotEqual(ship2 ,ship3);
        Assert.NotNull(ship3);
        Assert.True(ships.Any(s=>s == ship3));
        DestroyShip(ship3);

        var ship4 = player.GetFirstOrDefaultNotLocatedShip();
        Assert.Null(ship4);
    }

    [Test]
    public void IsFleetDestroyed_ShouldReturnTrueIfAllShipsAreDestroyed()
    {
        var player = new Player();
        var battleship = new Battleship();
        var destroyer1 = new Destroyer();
        var destroyer2 = new Destroyer();

        var ships = new IShip[] {battleship, destroyer1, destroyer2};
        player.CreateFleet(battleship, destroyer1, destroyer2);

        ships.All(s =>
        {
            DestroyShip(s);
            return true;
        });
        
        Assert.True(player.IsFleetDestroyed());
    }
    [Test]
    public void IsFleetDestroyed_ShouldReturnFalseIfNotAllShipsAreDestroyed()
    {
        var player = new Player();
        var battleship = new Battleship();
        var destroyer1 = new Destroyer();
        var destroyer2 = new Destroyer();

        player.CreateFleet(battleship, destroyer1, destroyer2);

        DestroyShip(battleship);
        
        Assert.False(player.IsFleetDestroyed());
    }
    private void DestroyShip(IShip ship)
    {
        var shipLocation = GetShipLocation(ship); 
        ship.SetSegments(shipLocation);
        ship.GetSegments().All(s =>
        {
            s.State = ShipSegment.ShipSegmentState.Hit;
            return true;
        });
    }

    private ShipLocation GetShipLocation(IShip ship)
    {
        var segments = new List<Coords>();
        for(int i =0; i< ship.Size; i++)
            segments.Add(new Coords('A', (uint)i + 1));
        return new ShipLocation(segments.ToArray());
    }
}