using BattleshipGame.Library.BoardItems;
using BattleshipGame.Library.PlayerItems;

namespace BattleshipGame.Tests;

public class BrainTests
{
    private List<char> _allowedColumns;
    private List<uint> _allowedRows;
    private Brain _brain;
    [SetUp]
    public void Setup()
    {
        _allowedColumns = new List<char> {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
        _allowedRows = new List<uint> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        var board = new Board();
        board.Setup((uint)_allowedColumns.Count);
        _brain = new Brain(board);
    }
    [Test]
    public void CalculateShipPlacement_ShouldCReturnCoordsInRangeOfAllowedColumnsAndRows()
    {
        var shipPlacement = _brain.CalculateShipPlacement();
        Assert.True(_allowedColumns.Any(c=>c == shipPlacement.Coords.Column));
        Assert.True(_allowedRows.Any(r=>r == shipPlacement.Coords.Row));
    }

    [Test]
    public void CalculateFireCoord_ShouldReturnCoordsInRangeOfAllowedColumnsAndRows()
    {
        var coords = _brain.CalculateFireCoord();
        Assert.NotNull(coords);
        Assert.True(_allowedColumns.Any(c=>c == coords?.Column));
        Assert.True(_allowedRows.Any(r=>r == coords?.Row));
    }

}