using BattleshipGame.Library.Abstraction;
using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.ShipItems;
namespace BattleshipGame.Tests;

public class ShipTests
{
    private IShip _battleship;
    private ShipLocation _shipLocation;
    private readonly Coords _c1 = new Coords('A', 1);
    private readonly Coords _c2 = new Coords('A', 2);
    private readonly Coords _c3 = new Coords('A', 3);
    private readonly Coords _c4 = new Coords('A', 4);
    private readonly Coords _c5 = new Coords('A', 5);
    private readonly Coords _c6 = new Coords('A', 6);
    private readonly Coords _missCoords = new Coords('B', 3);
    
    [SetUp]
    public void Setup()
    {
        _battleship = new Battleship();
        _shipLocation = new ShipLocation(new[] {_c1, _c2, _c3, _c4, _c5});
    }

    [Test]
    public void SetLocation_ShouldThrowExceptionWhenShipLocationSizeIsDifferentThanShipSize()
    {
        var location = new ShipLocation(new[] {_c1, _c2, _c3, _c4});
        Assert.Throws<Exception>(()=>_battleship.SetSegments(location));
        var location2 = new ShipLocation(new[] {_c1, _c2, _c3, _c4, _c5, _c6});
        Assert.Throws<Exception>(()=>_battleship.SetSegments(location));
    }
    [Test]
    public void GetShipSegments_ShouldReturnEmptyListWhenLocationIsNotSet()
    {
        Assert.Zero(_battleship.GetSegments().Count);
    }
    [Test]
    public void SetLocation_ShouldAddHealthyShipSegmentsForEachLocationCoords()
    {
        _battleship.SetSegments(_shipLocation);
        Assert.AreEqual(_battleship.GetSegments().Select(s=>s.Coords).ToList(), _shipLocation.shipSegments);
        Assert.True(_battleship.GetSegments().All(s=>s.State == ShipSegment.ShipSegmentState.Healthy));
    }
    [Test]
    public void IsDestroyed_ShouldBeFalseLocationIsNotSet()
    {
        Assert.False(_battleship.IsDestroyed());
    }
    [Test]
    public void IsDestroyed_ShouldBeFalseWhenNotAllShipSegmentsAreHit()
    {
        _battleship.SetSegments(_shipLocation);
        Assert.False(_battleship.IsDestroyed());
        _battleship.IsHit(_c1);
        Assert.False(_battleship.IsDestroyed());
        _battleship.IsHit(_c2);
        Assert.False(_battleship.IsDestroyed());
        _battleship.IsHit(_c4);
        Assert.False(_battleship.IsDestroyed());
    }
   
    [Test]
    public void IsDestroyed_ShouldBeTrueWhenAllShipSegmentsAreHit()
    {
        _battleship.SetSegments(_shipLocation);
        _battleship.IsHit(_c1);
        _battleship.IsHit(_c2);
        _battleship.IsHit(_c3);
        _battleship.IsHit(_c4);
        _battleship.IsHit(_c5);
        Assert.True(_battleship.IsDestroyed());
    }
    [Test]
    public void IsHit_ShouldBeTrueWhenHitCoordsIsTheSameAsCoordsOfAnySegmentOfShip()
    {
        _battleship.SetSegments(_shipLocation);
        var hit1 = _battleship.IsHit(_c1);
        Assert.True(hit1);
        var hit2 =  _battleship.IsHit(_c2);
        Assert.True(hit2);
        var hit3 = _battleship.IsHit(_c3);
        Assert.True(hit3);
        var hit4 = _battleship.IsHit(_c4);
        Assert.True(hit4);
        var hit5 = _battleship.IsHit(_c5);
        Assert.True(hit5);
        
        Assert.True(_battleship.IsDestroyed());
    }
    
    [Test]
    public void IsHit_ShouldBeFalseWhenHitCoordsIsNotTheSameAsCoordsOfAnySegmentOfShip()
    {
        _battleship.SetSegments(_shipLocation);
        var hit1 = _battleship.IsHit(_missCoords);
        Assert.IsFalse(hit1);
    }
    
    [Test]
    public void IsHit_ShouldNotDestroyShipWhenNotAllSegmentsHit()
    {
        _battleship.SetSegments(_shipLocation);
        _battleship.IsHit(_c1);
        _battleship.IsHit(_c1);
        _battleship.IsHit(_c1);
        _battleship.IsHit(_c1);
        _battleship.IsHit(_c2);
        Assert.IsFalse(_battleship.IsDestroyed());
    }
  
}