using System.Drawing;
using BattleshipGame.Library;
using BattleshipGame.Library.BoardItems;
using BattleshipGame.Library.CoordsItems;

namespace BattleshipGame.Tests;

public class CoordsRandomTests
{
    private readonly uint _boardSize = 10; 
    private List<char> _allowedColumns;
    private List<uint> _allowedRows;
    private CoordsRandom _coordsRandom;
    
    [SetUp]
    public void Setup()
    {
        _allowedColumns = new List<char> {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
        _allowedRows = new List<uint> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        var board = new Board();
        board.Setup(_boardSize);
        _coordsRandom = new CoordsRandom(board.Size);
    }

    [Test]
    public void GetCoords_ShouldGenerateCoordsInRangeOfAllowedColumnsAndRows()
    {
        for (int i = 0; i < 100000; i++)
        {
            var coords = _coordsRandom.GetCoords();
            Assert.True(_allowedColumns.Any(c=>c == coords.Column));
            Assert.True(_allowedRows.Any(r=>r == coords.Row));
        }
    }

    [Test]
    public void GetCoords_ShouldReturnCoordsFromListSizeOfOne()
    {
        Coords coords = new Coords('B', 4);
        var fields = new List<Field>(){new Field( Field.FieldState.Empty, coords)};
        Assert.AreEqual(coords, _coordsRandom.GetCoords(fields));
    }
    [Test]
    public void GetCoords_ShouldReturnAnyCoordFromListOfCoords()
    {
        Coords coords1 = new Coords('B', 4);
        Coords coords2 = new Coords('C', 7);
        var fields = new List<Field>(){new Field( Field.FieldState.Empty, coords1), new Field( Field.FieldState.Empty, coords2)};
        Assert.True(fields.Any(f=>f.Coords == _coordsRandom.GetCoords(fields)));
    }
    [Test]
    public void GetCoords_ShouldReturnNullWhenFieldListIsEmpty()
    {
        Assert.Null(_coordsRandom.GetCoords(new List<Field>()));
    }
}