using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace BattleshipGame;

public partial class Board : UserControl
{
    public Board()
    {
        InitializeComponent();
        LayoutRoot.DataContext = this;
    }
    
    public IEnumerable ItemsSource
    {
        get { return (IEnumerable)GetValue(ItemsSourceProperty); }
        set { SetValue(ItemsSourceProperty, value); }
    }

    public static readonly DependencyProperty ItemsSourceProperty =
        DependencyProperty.Register("ItemsSource", typeof(IEnumerable),
            typeof(Board), new PropertyMetadata(null));

    public int Size     
    {
        get { return (int)GetValue(SizeProperty); }
        set { SetValue(SizeProperty, value); }
    }
    public static readonly DependencyProperty SizeProperty =
        DependencyProperty.Register("Size", typeof(int),
            typeof(Board), new PropertyMetadata(null));
    
    public bool Enabled
    {
        get { return (bool)GetValue(EnabledProperty); }
        set { SetValue(EnabledProperty, value); }
    }
    public static readonly DependencyProperty EnabledProperty =
        DependencyProperty.Register("Enabled", typeof(int),
            typeof(Board), new PropertyMetadata(null));

    
}