using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace BattleshipGame;

public abstract class BaseViewModel : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler? PropertyChanged;
    protected virtual void RaisePropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    protected bool Property<T>(ref T fieldReference, T newValue, [CallerMemberName] string? propertyName = null)
    {
        if (Equals(fieldReference, newValue)) return false;
        fieldReference = newValue;
        RaisePropertyChanged(propertyName);
        return true;
    }
}