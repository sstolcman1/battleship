using System.Windows;

namespace BattleshipGame;

public partial class GameViewModel
{
    private Visibility _selectBoardSizePanelSizeVisibility = Visibility.Collapsed;
    private Visibility _boardPanelVisibility = Visibility.Collapsed;
    private Visibility _placeShipsPanelPanelVisibility = Visibility.Collapsed;
    private Visibility _gameOverPanelVisibilty = Visibility.Collapsed;
    public Visibility SelectBoardSizePanelVisibilty
    {
        get => _selectBoardSizePanelSizeVisibility;
        set => Property(ref _selectBoardSizePanelSizeVisibility, value);
    }
    public Visibility GameOverPanelVisibilty
    {
        get => _gameOverPanelVisibilty;
        set => Property(ref _gameOverPanelVisibilty, value);
    }
    public Visibility BoardPanelVisibility
    {
        get => _boardPanelVisibility;
        set => Property(ref _boardPanelVisibility, value);
    }
    public Visibility PlaceShipsPanelVisibility
    {
        get => _placeShipsPanelPanelVisibility;
        set => Property(ref _placeShipsPanelPanelVisibility, value);
    }
}