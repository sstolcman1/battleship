using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Media;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using BattleshipGame.Library.Abstraction;
using BattleshipGame.Library.BoardItems;
using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.GameItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame;

public partial class GameViewModel : BaseViewModel
{
    private readonly IGame _game;
  
    private uint _selectedBoardSize = 10;
    private List<FieldViewModel> _computerBoard;
    private List<FieldViewModel> _playerBoard;
    private bool _started = false;
    private string _statusInfo;
    private IShip? _shipToPlace;
    private string _shipToPlaceInfo;
    
    private bool _computerBoardEnabled = false;
    private bool _playerBoardEnabled = true;
    private string _selectedOrientation;
    private string _gameOverText;
    public bool _isShipHorizontal
    {
        get => SelectedOrientation == Orientations[0]; 
    }

    public List<string> Orientations => new List<string>(){"Horizontal", "Vertical"};
    public List<uint> BoardSizes => new List<uint>() {10, 12, 15};
    
    public string GameOverText
    {
        get => _gameOverText;
        set => Property(ref _gameOverText, value);
    }
    public uint SelectedBoardSize
    {
        get => _selectedBoardSize;
        set => Property(ref _selectedBoardSize, value);
    }
    public string ShipToPlaceInfo
    {
        get => _shipToPlaceInfo;
        set => Property(ref _shipToPlaceInfo, value);
    }
    public string SelectedOrientation
    {
        get => _selectedOrientation;
        set => Property(ref _selectedOrientation, value);
    }
    public List<FieldViewModel> ComputerBoard
    {
        get => _computerBoard;
        set => Property(ref _computerBoard, value); 
    }
    public string StatusInfo
    {
        get => _statusInfo;
        set => Property(ref _statusInfo, value);
    }
    
    public bool ComputerBoardEnabled
    {
        get => _computerBoardEnabled;
        set => Property(ref _computerBoardEnabled, value);
    }
    public bool PlayerBoardEnabled
    {
        get => _playerBoardEnabled;
        set => Property(ref _playerBoardEnabled, value);
    }
    public List<FieldViewModel> PlayerBoard
    {
        get => _playerBoard;
        set => Property(ref _playerBoard, value);
    }
  
    private SoundPlayer playerFire;
    private SoundPlayer playerExplosion;
    private SoundPlayer playerSuccess;
    private SoundPlayer playerFail;
  

        private void LoadSounds()
        {
            playerFire = new("sounds\\fire.wav");
            playerFire.Load();
            playerExplosion = new("sounds\\explosion.wav");
            playerExplosion.Load();
            playerSuccess = new("sounds\\success-fanfare.wav");
            playerSuccess.Load();
            playerFail = new("sounds\\failfare.wav");
            playerFail.Load();
        }
    public GameViewModel(IGame game)
    {
        _game = game;
        
        
        SelectedOrientation = Orientations[0];
        State = GameState.BoardSizeSelecting;
        LoadSounds();
    }

    private void CreateBoards()
    {
        var boards = _game.CreateBoards(_selectedBoardSize);
        
        ComputerBoard = CreateBoard(boards.computerBoard, true);
        PlayerBoard = CreateBoard(boards.playerBoard);
    }
    
    private void PlaceShip()
    {
        _shipToPlace = _game.GetNextPlayerShipToPlace();
        if (_shipToPlace is null)
        {
            State = GameState.ShipPlacingCompleted;
            return;
        }
        ShipToPlaceInfo = $"Place {_shipToPlace.Name} on player's board";
    }
    private List<FieldViewModel> CreateBoard(IBoard board, bool computerBoard = false)
    {
        var fields = new List<FieldViewModel>();
        for(int r= 0; r<_selectedBoardSize; r++)
        {
            for (int c = 0; c < _selectedBoardSize; c++)
            {
                char column = (char) ('A' + c);
                uint row = (uint) r + 1;
                fields.Add(new FieldViewModel(){OnComputerBoard = computerBoard, Field = board.Fields.First(f=>f.Coords == new Coords(column, row)), Column = column, Row = row, Background = new SolidColorBrush(Colors.LightBlue)});
            }
        }

        return fields;
    }

    #region Commands
    //PlayAgainCmd
    public ICommand PlayAgainCmd { get { return new RelayCommand(OnPlayAgain, ()=>true); } }
    private void OnPlayAgain()
    {
        _started = false;
        State = GameState.BoardSizeSelecting;
    }
    public ICommand MouseLeaveCmd { get { return new RelayCommand<FieldViewModel>(OnMouseLeave, canExecute: (FieldViewModel f)=>true); } }
    private void OnMouseLeave(FieldViewModel? fieldViewModel)
    {
        if(fieldViewModel is null) return;

        if (State == GameState.ShipPlacing && _shipToPlace is not null)
        {
            
            var shipFields = GetShipFields(fieldViewModel);
            shipFields.All(f =>
            {
                // f.Background = shipLocation is null ? FieldViewModel.ColorShipHit : FieldViewModel.ColorShipLocated;
                if (f.ShipLocated)
                    f.Background = FieldViewModel.ColorShipLocated;
                else
                    f.Background = FieldViewModel.ColorEmpty;
                return true;
            });
            return;
        }

        if (State == GameState.PlayerTurn)
        {
            if(!fieldViewModel.OnComputerBoard) return;
            if(fieldViewModel.Field.State == Field.FieldState.Hit)
                fieldViewModel.Background = FieldViewModel.ColorShipHit;
            else if(fieldViewModel.Field.State == Field.FieldState.Fired)
                fieldViewModel.Background = FieldViewModel.ColorShootAt;
            else
                fieldViewModel.Background = FieldViewModel.ColorEmpty;
            return;
        }
    }
    public ICommand MouseOverCmd { get { return new RelayCommand<FieldViewModel>(OnMouseOver, canExecute: (FieldViewModel f)=>true); } }

    private void OnMouseOver(FieldViewModel fieldViewModel)
    {
        if(fieldViewModel is null) return;
        if (State == GameState.ShipPlacing && _shipToPlace is not null)
        {
            var shipLocation = _game.CanPlayerPlaceShip(_shipToPlace,
                new ShipPlacement(fieldViewModel.Field.Coords,
                    _isShipHorizontal ? IShip.ShipOrientation.Horizontal : IShip.ShipOrientation.Vertical));
            var shipFields = GetShipFields(fieldViewModel);
            shipFields.All(f =>
            {
                f.Background = shipLocation is null
                        ? FieldViewModel.ColorShipHit
                        : FieldViewModel.ColorShipLocated;
                return true;
            });
            return;
        }

        // if (shipFields.Count < _shipToPlace.Size)
        //     {
        //         shipFields.All(f =>
        //         {
        //             f.Background = FieldViewModel.ColorShipHit;
        //             return true;
        //         });
        //     }
        //     else
        //     {
        //         shipFields.All(f =>
        //         {
        //             if (f.ShipLocated)
        //                 f.Background = FieldViewModel.ColorShipHit;
        //             else
        //                 f.Background = FieldViewModel.ColorShipLocated;
        //             return true;
        //         });
        //     }
        //
        //     return;
        // }

        if (State == GameState.PlayerTurn)
        {
            if(!fieldViewModel.OnComputerBoard) return;
            if(fieldViewModel.Field.State is Field.FieldState.Empty)
                fieldViewModel.Background = FieldViewModel.ColorShootAt;
            return;
        }
    }

    public ICommand StartCmd { get { return new RelayCommand(OnStart, CanStart); } }
    private void OnStart()
    {
        _started = true;
        State = GameState.BoardSizeSelectingCompleted;
    }

    private bool CanStart()
    {
        return !_started;
    }
    
    public RelayCommand<FieldViewModel> ClickCmd { get { return new RelayCommand<FieldViewModel>(OnClickCmd, CanClickCmd); } }

    private void OnClickCmd(FieldViewModel field)
    {
        if (field is null) return;
        if (State is GameState.ShipPlacing)
        {
            var fields = GetShipFields(field);
            if (fields.Count < _shipToPlace.Size || fields.Any(f => f.ShipLocated))
            {
                SystemSounds.Hand.Play();
                return;
            }

            fields.All(f =>
            {
                f.Background = FieldViewModel.ColorShipLocated;
                f.ShipLocated = true;
                return true;
            });
            var shipLocation = _game.CanPlayerPlaceShip(_shipToPlace,
                new ShipPlacement(field.Field.Coords,
                    _isShipHorizontal ? IShip.ShipOrientation.Horizontal : IShip.ShipOrientation.Vertical));
            if (shipLocation is null)
            {
                SystemSounds.Hand.Play();
            }
            else
            {
                _game.PlacePlayerShip(_shipToPlace, shipLocation);
            }

            PlaceShip();
            return;
        }

        if (State is GameState.PlayerTurn)
        {
            if(field.Field.State is not Field.FieldState.Empty) return;
            var shootResult = _game.PlayerShoot(field.Field.Coords);
            SetBackground(field, shootResult);
            PlaySound(shootResult);
            StatusInfo = $"You shoot at {shootResult.Coords} and {shootResult.Result}";
            if(shootResult.Result== FireResult.Destroyed)
                if (_game.IsComputerFleetDestroyed())
                {
                    playerSuccess.Play();
                    GameOverText = "You won!";
                    ChangeState(GameState.GameOver);
                    return;
                }

            //State = GameState.ComputerTurn;
            ChangeState(GameState.ComputerTurn);
        }
    }

    private async Task ChangeState(GameState state)
    {
        State = GameState.Waiting;
        await Task.Delay(1000);
        State = state;
    }
    private void PlaySound(ShootResult shootResult)
    {
        
        switch (shootResult.Result)
        {
            case FireResult.Missed:
                playerFire.Play();
                break;
            case FireResult.Hit:
                playerExplosion.Play();
                break;
            case FireResult.Destroyed:
                playerExplosion.Play();
                playerExplosion.Play();
                break;
            // FireResult.Hit => Console.WriteLine("Play hit sound"),
            // FireResult.Destroyed => Console.WriteLine("Play destroyed sound"),
            // FireResult.AlreadyHit => Console.WriteLine("Play already hit sound"),
        };
    }

    private void SetBackground(FieldViewModel field, ShootResult shootResult)
    {
        if (shootResult.Result is FireResult.Hit or FireResult.Destroyed) 
            field.Background = FieldViewModel.ColorShipHit;

        if (shootResult.Result is FireResult.Missed)
            field.Background = FieldViewModel.ColorShootAt;
    }
    private bool CanClickCmd(FieldViewModel field)
    {
        return true;
        // if (field is not null) return true;
        // if (State == GameState.ShipPlacing)
        //     return true;
        // return field.Background.ToString() == new SolidColorBrush(Colors.LightBlue).ToString();
    }

    private List<FieldViewModel> GetShipFields(FieldViewModel field)
    {
        if (_shipToPlace is null)
            return new List<FieldViewModel>();
        if (_isShipHorizontal)
        {
            return _playerBoard.Where(f => f.Row == field.Row && (f.Column >= field.Column && f.Column < field.Column + _shipToPlace.Size))
                .ToList();
        }
        return _playerBoard.Where(f => f.Column == field.Column && (f.Row >= field.Row && f.Row < field.Row + _shipToPlace.Size))
            .ToList();
    }
    
    #endregion Commands
}