using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using BattleshipGame.Library.BoardItems;
using BattleshipGame.Library.CoordsItems;

namespace BattleshipGame;

public class FieldViewModel : BaseViewModel
{
    public static readonly SolidColorBrush ColorShipLocated = new SolidColorBrush(Colors.Green);
    public static readonly SolidColorBrush ColorShipHit = new SolidColorBrush(Colors.Red);
    public static readonly SolidColorBrush ColorShootAt = new SolidColorBrush(Colors.Coral);
    public static readonly SolidColorBrush ColorEmpty = new SolidColorBrush(Colors.LightBlue);

    private Brush _background = ColorEmpty;
    private bool _shipLocated = false;
    private string _status = string.Empty;
    public char Column { get; set; }
    public uint Row { get; set; }
    public Field Field { get; set; }
    public bool OnComputerBoard { get; set; } 
    
    public Brush Background
    {
        get => _background;
        set => Property(ref _background, value);
    }
    public bool ShipLocated
    {
        get => _shipLocated;
        set => Property(ref _shipLocated, value);
    }

    public string Status
    {
        get => _status;
        set => Property(ref _status, value);
    }

    public override string ToString()
    {
        return $"Cell: ({Column}, {Row}), Status: {Status}";
    }
    
    #region EventHandles
    public event PropertyChangedEventHandler? PropertyChanged;
    protected virtual void RaisePropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
    #endregion EventHandles
}