using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using BattleshipGame.Library.Abstraction;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame;

public partial class GameViewModel
{
    public enum GameState {None, BoardSizeSelecting, BoardSizeSelectingCompleted, ShipPlacing, ShipPlacingCompleted, ComputerTurn, PlayerTurn, GameOver,
        Waiting
    }

    private GameState _state = GameState.None;
    public GameState State
    {
        get { return _state;}
        set
        {
            if (_state != value)
            {
                _state = value;
                RaisePropertyChanged();

                switch (_state)
                {
                    case GameState.BoardSizeSelecting:
                        SetBoardSizeSelectingState();
                        break;
                    case GameState.BoardSizeSelectingCompleted:
                        SetBoardSizeSelectingCompletedState();
                        break;
                    case GameState.ShipPlacing:
                        SetShipPlacingState();
                        break;
                    case GameState.ShipPlacingCompleted:
                        SetShipPlacingCompletedState();
                        break;
                    case GameState.ComputerTurn:
                        SetComputerTurnState();
                        break;
                    case GameState.PlayerTurn:
                        SetPlayerTurnState();
                        break;
                    case GameState.GameOver:
                        SetGameOverState();
                        break;
                    case GameState.Waiting:
                        SetWaitingState();
                        break;
                    default:
                        throw new NotImplementedException($"Game state: {_state} not implemented");
                }
            }
        }
    }

    private void SetWaitingState()
    {
        ComputerBoardEnabled = false;
        PlayerBoardEnabled = false;
    }

    private void SetBoardSizeSelectingState()
    {
        _game.CreateGame();
        _game.CreateShips();
        
        ComputerBoardEnabled = false;
        PlayerBoardEnabled = false;
        SelectBoardSizePanelVisibilty = Visibility.Visible;
        GameOverPanelVisibilty = Visibility.Collapsed;
        StatusInfo = "Select board size, you want to generate.";
    }

    private void SetBoardSizeSelectingCompletedState()
    {
        CreateBoards();
        SelectBoardSizePanelVisibilty = Visibility.Collapsed;
        
        StatusInfo = "Computer place ships...";
        _game.PlaceComputerShips();
        
        State = GameState.ShipPlacing;
        BoardPanelVisibility = Visibility.Visible;
    }
    private void SetShipPlacingState()
    {
        StatusInfo = "Player place ships...";
        ComputerBoardEnabled = false;
        PlayerBoardEnabled = true;
        PlaceShipsPanelVisibility = Visibility.Visible;
        
        PlaceShip();        
    }

    private void SetShipPlacingCompletedState()
    {
        ComputerBoardEnabled = false;
        PlayerBoardEnabled = false;
        PlaceShipsPanelVisibility = Visibility.Collapsed;
        BoardPanelVisibility = Visibility.Visible;
        State = GameState.ComputerTurn;
    }
    private void SetComputerTurnState()
    {
        ComputerBoardEnabled = false;
        PlayerBoardEnabled = false;

        var shootResult = _game.ComputerShoot();
        StatusInfo = $"Computer shoot at {shootResult.Coords} and {shootResult.Result}";
        var field = _playerBoard.First(f => f.Field.Coords == shootResult.Coords);
        
        if (shootResult.Result == FireResult.Hit || shootResult.Result== FireResult.Destroyed)
            field.Background = FieldViewModel.ColorShipHit;
        if (shootResult.Result == FireResult.Missed)
            field.Background = FieldViewModel.ColorShootAt;

        if (_game.IsPlayerFleetDestroyed())
        {
            playerFail.Play();
            GameOverText = "You lost!";
            State = GameState.GameOver;
            return;
        }
        State = GameState.PlayerTurn;
    }

    private void SetPlayerTurnState()
    {
        PlayerBoardEnabled = false;
        ComputerBoardEnabled = true;
    }

    private void SetGameOverState()
    {
        StatusInfo = "End of game";
        ComputerBoardEnabled = false;
        PlayerBoardEnabled = false;
        GameOverPanelVisibilty = Visibility.Visible;
        //BoardPanelVisibility = Visibility.Collapsed;
    }
}