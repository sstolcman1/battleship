using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.GameItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Library.Abstraction;

public interface IGame
{
    void CreateGame();
    (IBoard computerBoard, IBoard playerBoard) CreateBoards(uint size);
    void CreateShips();
    void PlaceComputerShips();

    IShip? GetNextPlayerShipToPlace();
    ShipLocation? CanPlayerPlaceShip(IShip ship, ShipPlacement shipPlacement);
    void PlacePlayerShip(IShip ship, ShipLocation shipLocation);

    ShootResult? ComputerShoot();
    ShootResult PlayerShoot(Coords coords);

    bool IsPlayerFleetDestroyed();
    bool IsComputerFleetDestroyed();
    
}