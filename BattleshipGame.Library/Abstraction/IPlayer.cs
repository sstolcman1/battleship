using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Library.Abstraction;

public interface IPlayer
{

    void CreateFleet(params IShip[] ships);
    bool IsFleetDestroyed();
    IShip? GetFirstOrDefaultNotLocatedShip();
}