using BattleshipGame.Library.BoardItems;
using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Library.Abstraction;

public interface IBoard
{
    uint Size { get; }
    List<Field> Fields { get; }
    List<IShip> Ships { get; }

    void Setup(uint size);
    FireResult FireAt(CoordsItems.Coords coords);
    ShipLocation? CanPlaceShip(IShip ship, ShipPlacement shipPlacement);
    void PlaceShip(IShip ship, ShipLocation shipLocation);
    List<Field> GetNotFiredFields();
}