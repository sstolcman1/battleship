using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Library.Abstraction;

public interface IShip
{
    public enum ShipOrientation
    {
        Vertical,
        Horizontal
    };

    int Size  { get; }
    string Name { get; }
    ShipOrientation Orientation { get; }

    void SetSegments(ShipLocation shipLocation);
    bool IsDestroyed();
    bool IsHit(Coords coord);
    List<ShipSegment> GetSegments();
}