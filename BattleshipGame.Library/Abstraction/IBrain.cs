using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Library.Abstraction;

public interface IBrain
{
    ShipPlacement CalculateShipPlacement();
    Coords? CalculateFireCoord();
}