namespace BattleshipGame.Library.CoordsItems;

public record ShipLocation(Coords[] shipSegments);