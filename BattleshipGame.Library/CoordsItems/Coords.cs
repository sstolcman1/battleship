namespace BattleshipGame.Library.CoordsItems;

public record Coords
{
    public char Column { get; set; }
    public uint Row { get; set; }

    public override string ToString()
    {
        return $"[{Column}{Row}]";
    }

    public Coords(char column, uint row)
    {
        Column = column;
        Row = row;
    }
    public static Coords Copy(Coords from)
    {
        return new Coords(from.Column, from.Row);
    }
}