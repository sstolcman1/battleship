using BattleshipGame.Library.Abstraction;
using BattleshipGame.Library.BoardItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Library.CoordsItems;

public class CoordsRandom
{
    private readonly Random _rnd;
    private readonly uint _boardSize;
    public CoordsRandom(uint boardSizeSize)
    {
        _rnd = new Random(DateTime.Now.Millisecond);
        _boardSize = boardSizeSize;
    }

    public Coords GetCoords()
    {
        return new Coords(Convert.ToChar('A' + _rnd.Next((int) _boardSize)),
            (uint) _rnd.Next(0, (int) _boardSize) + 1);
    }
    public Coords? GetCoords(List<Field> fields)
    {
        return fields.Count == 0 ? null : fields[_rnd.Next(fields.Count)].Coords;
    }
    public IShip.ShipOrientation GetOrientation()
    {
        return _rnd.Next(0, 2) == 0 ? IShip.ShipOrientation.Horizontal : IShip.ShipOrientation.Vertical;
    }
    
}