using BattleshipGame.Library.Abstraction;
using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Library.BoardItems;

public class CollisionDetector
{
    public bool Collision(List<IShip> ships, Coords[] location)
    {
        foreach (var ship in ships)
        {
            var coords = ship.GetSegments().Select(s=>s.Coords);

            if (location.Any(l => coords.Any(c => c == l)))
                return true;
            // foreach (var l in location)
            // {
            //     if (coords.Any(c => c == l))
            //         return true;
            // }
        }
        return false;
    }
}