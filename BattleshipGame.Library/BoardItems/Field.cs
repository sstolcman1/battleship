using BattleshipGame.Library.CoordsItems;

namespace BattleshipGame.Library.BoardItems;

public record Field
{
    public enum FieldState {Empty, Fired, Hit}

    public Field(FieldState state, Coords coords)
    {
        State = state;
        Coords = coords;
    }
    public FieldState State { get; set; } = FieldState.Empty;

    public Coords Coords { get; set; }
    
    public void ClearState()
    {
        State = FieldState.Empty;
    }

    public override string ToString()
    {
        return $"({Coords.Column}, {Coords.Row})";
    }
}