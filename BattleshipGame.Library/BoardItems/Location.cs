using BattleshipGame.Library.Abstraction;
using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Library.BoardItems;

public class Location
{
    public Coords[] CalculateShipLocation(ShipPlacement shipPlacement, int shipSize)
    {
        Coords[] location = new Coords[shipSize];
        if (shipPlacement.Orientation == IShip.ShipOrientation.Vertical)
        {
            var h = shipPlacement.Coords.Row;
            for (int i=0; i<location.Length; i++)
            {
                location[i] = new Coords(shipPlacement.Coords.Column, h++);
            }
        }
        else
        {
            var c = shipPlacement.Coords.Column;
            for (int i=0; i<location.Length; i++)
            {
                location[i] = new Coords(c, shipPlacement.Coords.Row);
                c = Convert.ToChar(Convert.ToInt16(c) + 1);
            }
        }

        return location;
    }
}