using BattleshipGame.Library.Abstraction;
using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Library.BoardItems;

public class Board : IBoard
{
    public uint Size { get;  private set; }
    
    public List<IShip> Ships { get; private set; }
    public List<Field> Fields { get; private set; }
    private Location _location;
    private CollisionDetector _collisionDetector;
    public Char MaxColumn { get => (char)('A' + Size); }
    public void Setup(uint size)
    {
        Size = size;
        Ships = new List<IShip>();
        Fields = new List<Field>();
        _location = new();
        _collisionDetector = new();
        
        CreateBoardFields();
    }

    private void CreateBoardFields()
    {
        for(int c= 0; c<Size; c++)
        {
            for (uint r = 0; r < Size; r++)
            {
                Fields.Add(new Field(Field.FieldState.Empty, new Coords((char)('A' + c), r + 1)));
            }
        }
    }
    public List<Field> GetNotFiredFields()
    {
        return Fields.Where(f => f.State == Field.FieldState.Empty).ToList();
    }

    public FireResult FireAt(Coords coords)
    {
        var field = Fields.FirstOrDefault(f => f.Coords == coords);

        if (field is null)
            throw new Exception("Fire out of board!");

        if (field.State != Field.FieldState.Empty)
            return FireResult.AlreadyHit;

        var ship = Ships.FirstOrDefault(ship => ship.IsHit(coords));
        if (ship is not null)
        {
            field.State = Field.FieldState.Hit;
            return ship.IsDestroyed() ? FireResult.Destroyed : FireResult.Hit;
        }

        field.State = Field.FieldState.Fired;
        return FireResult.Missed;
    }

    public ShipLocation? CanPlaceShip(IShip ship, ShipPlacement shipPlacement)
    {
        var shipSegments = _location.CalculateShipLocation(shipPlacement, ship.Size);
        if (shipSegments.Any(c => c.Column >= MaxColumn || c.Row > Size))
            return null;
        if (_collisionDetector.Collision(Ships, shipSegments))
            return null;
        return new ShipLocation(shipSegments);
    }

    public void PlaceShip(IShip ship, ShipLocation shipLocation)
    {
        ship.SetSegments(shipLocation);
        Ships.Add(ship);
    }

    
        
   
}