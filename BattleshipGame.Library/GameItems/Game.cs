using BattleshipGame.Library.PlayerItems;
using BattleshipGame.Library.Abstraction;
using BattleshipGame.Library.BoardItems;
using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Library.GameItems;

public class Game : IGame
{
    private const uint BoardSize = 10;
    
    private IBoard _computerBoard;
    private IBoard _playerBoard;
    private IPlayer _computer;
    private IPlayer _player;
    private IBrain _brain;
    
    public void CreateGame()
    {
        _computerBoard = new Board();
        _playerBoard = new Board();
        _computer = new Player();
        _player = new Player();
    }
    public (IBoard computerBoard, IBoard playerBoard) CreateBoards(uint size = BoardSize)
    {
        _computerBoard.Setup(size);
        _playerBoard.Setup(size);
        _brain = new Brain(_playerBoard);
        return (_computerBoard, _playerBoard);
    }

    public void CreateShips()
    {
        _computer.CreateFleet(new Battleship(), new Destroyer(), new Destroyer());
        _player.CreateFleet(new Battleship(), new Destroyer(), new Destroyer());
    }

    public void PlaceComputerShips()
    {
        while (true)
        {
            var ship = _computer.GetFirstOrDefaultNotLocatedShip();
            if (ship is null)
                break;
            while (true)
            {
                var shipPlacement = _brain.CalculateShipPlacement();
                var shipSegments = _computerBoard.CanPlaceShip(ship, shipPlacement);
                if (shipSegments is null) continue;
                _computerBoard.PlaceShip(ship, shipSegments);
                break;
            }
        }
    }
    

    public IShip? GetNextPlayerShipToPlace()
    {
        return _player.GetFirstOrDefaultNotLocatedShip();
    }

    public ShipLocation? CanPlayerPlaceShip(IShip ship, ShipPlacement shipPlacement)
    {
        return _playerBoard.CanPlaceShip(ship, shipPlacement);
    }

    public void PlacePlayerShip(IShip ship, ShipLocation shipSegment)
    {
        _playerBoard.PlaceShip(ship, shipSegment);
    }

    public ShootResult? ComputerShoot()
    {
        var coords = _brain.CalculateFireCoord();
        return coords is null ? null : new ShootResult(coords, _playerBoard.FireAt(coords));
    }

    public ShootResult PlayerShoot(Coords coords)
    {
        return new ShootResult(coords, _computerBoard.FireAt(coords));
    }

    public bool IsPlayerFleetDestroyed() => _player.IsFleetDestroyed();
    public bool IsComputerFleetDestroyed() => _computer.IsFleetDestroyed();
    
}