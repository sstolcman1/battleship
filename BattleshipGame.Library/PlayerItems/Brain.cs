using System.Diagnostics.CodeAnalysis;
using BattleshipGame.Library.Abstraction;
using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Library.PlayerItems;

public class Brain : IBrain
{
    private readonly IBoard _playerBoard;
    private readonly CoordsRandom _coordsRandom;
    public Brain(IBoard playerBoard)
    {
        _playerBoard = playerBoard;
        _coordsRandom = new CoordsRandom(playerBoard.Size);
    }

    public ShipPlacement CalculateShipPlacement()
    {
        var orientation = _coordsRandom.GetOrientation();
        return new ShipPlacement(_coordsRandom.GetCoords(), orientation);
    }

    public Coords? CalculateFireCoord()
    {
        var fields = _playerBoard.GetNotFiredFields();
        return !fields.Any() ? null : _coordsRandom.GetCoords(fields);
    }
}