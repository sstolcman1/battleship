using BattleshipGame.Library.Abstraction;
using BattleshipGame.Library.CoordsItems;
using BattleshipGame.Library.ShipItems;

namespace BattleshipGame.Library.PlayerItems;

public class Player : IPlayer
{
    private readonly List<IShip> _ships;
    public Player()
    {
        _ships = new List<IShip>();
    }

    public void CreateFleet(params IShip[] ships)
    {
        _ships.Clear();
        _ships.AddRange(ships);
    }
    public bool IsFleetDestroyed()
    {
        return _ships.All(ship => ship.IsDestroyed());
    }
    public IShip? GetFirstOrDefaultNotLocatedShip()
    {
        return _ships.FirstOrDefault(ship => ship.GetSegments().Count == 0);
    }
}