using BattleshipGame.Library.Abstraction;
using BattleshipGame.Library.CoordsItems;

namespace BattleshipGame.Library.ShipItems;

public abstract class Ship : IShip
{
    public int Size { get; }
    public string Name { get; }
    public IShip.ShipOrientation Orientation { get; private set; }
    private readonly List<ShipSegment> _shipSegments;
   
    
    public Ship(string name, int size)
    {
        Size = size;
        Name = name;
        _shipSegments = new List<ShipSegment>(size);
    }

    public void SetSegments(ShipLocation shipLocation)
    {
        if (shipLocation.shipSegments.Length != Size)
            throw new Exception("Ship location does not contain expected segments which is: {Size}");
        _shipSegments.AddRange(shipLocation.shipSegments.Select(s=> new ShipSegment(){Coords = s, State = ShipSegment.ShipSegmentState.Healthy}));
    }

    public bool IsDestroyed()
    {
        return _shipSegments.Count > 0 && _shipSegments.All(s => s.State == ShipSegment.ShipSegmentState.Hit);
    }

    public bool IsHit(Coords coord)
    {
        var segment = _shipSegments.FirstOrDefault(s => s.Coords == coord);
        if (segment is not null)
        {
            segment.State = ShipSegment.ShipSegmentState.Hit;
            return true;
        }
        return false;
    }

    public List<ShipSegment> GetSegments()
    {
        return _shipSegments;
    }

    public override string ToString()
    {
        return $"{Name} {string.Join(" ", _shipSegments.Select(s=>  s.Coords + (s.State== ShipSegment.ShipSegmentState.Hit? "*" : " ")).ToList())}";
    }
    
}