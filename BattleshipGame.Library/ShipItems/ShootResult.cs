using BattleshipGame.Library.CoordsItems;

namespace BattleshipGame.Library.ShipItems;

public enum FireResult {Missed, Hit, Destroyed, AlreadyHit}

public record ShootResult(Coords Coords, FireResult Result);
