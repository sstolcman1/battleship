using BattleshipGame.Library.CoordsItems;

namespace BattleshipGame.Library.ShipItems;

public class ShipSegment
{
    public enum ShipSegmentState
    {
        Healthy,
        Hit
    };
    public Coords Coords { get; set; }
    public ShipSegmentState State { get; set; } = ShipSegmentState.Healthy;
}