using BattleshipGame.Library.Abstraction;
using BattleshipGame.Library.CoordsItems;

namespace BattleshipGame.Library.ShipItems;

public record ShipPlacement(Coords Coords, IShip.ShipOrientation Orientation);
